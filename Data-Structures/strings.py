selfish = "me, me, me"

nums = '12345678910'
print(selfish[0])
print(selfish[1:])
print(selfish[:2])

print(selfish[2:])
#reverse of the string
print(selfish[::-1])
#skip by two
print(nums[::-2])

print("\n")
q1 = "I am a string"
print(f"before change {q1}")
q2 = q1.replace("string", "python")
print(q2)