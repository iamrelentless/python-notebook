'''
python has a built-in function to read files much like C++ has fstream
'''

#name of file, write, what to write
open('test.txt', 'w').write('Hello World!')

#read a file
my_file = open('test.txt')

#open function can only read a file once
#calling it again won't read the file
#it has an open cursor so use seek to read the file again
print(my_file.read())
my_file.seek(0) #moves cursor back to the beginning of the file

#another option would be to use readline instead of read
print(my_file.readline())


my_file = open('test.txt')

#there is also readlines
print(my_file.readlines())

#don't forget to close the file
my_file.close()