import os


logo = '''
           _            _       _   
          | |          | |     | |            
  ___ __ _| | ___ _   _| | __ _| |_ ___  _ __ 
 / __/ _` | |/ __| | | | |/ _` | __/ _ \| '__|
| (_| (_| | | (__| |_| | | (_| | || (_) | |   
 \___\__,_|_|\___|\__,_|_|\__,_|\__\___/|_|

 '''


def add(n1, n2):
    return n1 + n2


def subtract(n1, n2):
    return n1 - n2


def multiply(n1, n2):
    return n1 * n2


def divide(n1, n2):
    return n1 / n2


operations = {
    "+": add,
    "-": subtract,
    "*": multiply,
    "/": divide
}


def calculator():
    '''
    this function runs the whole program and asks the user for 
    number and operator inputs then does calculates depending on 
    the chosen operator.  
    '''
    print(logo)

    n1 = float(input("Enter number: "))
    for symbol in operations:
        print(symbol)
    should_continue = True

    while should_continue:
        operator = input("Choose operation: ")
        n2 = float(input("Enter number: "))
        calculation_function = operations[operator]
        answer = calculation_function(n1, n2)
        print(f"{n1} {operator} {n2} = {answer}")

        if input(f"'Y' to continue calculating with {answer}, or 'n' to stop ") == 'y':
            n1 = answer
        else:
            should_continue = False
            os.system('cls' if os.name == 'nt' else 'clear')
            #calculator()

#call the function and start calculating
calculator()