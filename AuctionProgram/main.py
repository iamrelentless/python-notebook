from art import logo
import os
print(logo)

bids = {} #dictionary for bids
end_auction = False

def get_highest_bidder(bid_record):
    '''
    gets the highest bidder by taking argument bid_record as a dictionary
    puts highest bidders name into bid_record and store highest bid as a pair (kvp)
    '''
    highest = 0
    winner = ""

    '''
    for loop to iterate through bid_record and find highest bidder
    '''
    for bidder in bid_record:

        bid_amt = bid_record[bidder]

        #if bid_amt is greater than highest, set highest to bid_amt
        if bid_amt > highest:
            highest = bid_amt #stores highest bid into bid_amt
            winner = bidder #puts bidder in index
    print(f"Winner is {winner} with a bid of ${highest}" )

while not end_auction:

    name = input("Enter name: ")
    price = int(input("Enter your bid: "))
    bids[name] = price
    add_more_bidders = input("Any more bidders? yes/no?\n").lower()

    #if add_more_bidders is no, set end_auction to True
    if add_more_bidders == 'no':
        end_auction = True
        get_highest_bidder(bids)
    elif add_more_bidders == 'yes':
        #if add_more_bidders is yes, continue loop
        os.system('cls' if os.name == 'nt' else 'clear')