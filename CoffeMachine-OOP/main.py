from menu import Menu, MenuItem
# from the coffeemaker file
from coffee_maker import CoffeeMaker
# from moneymachine file
from money_machine import MoneyMachine

#create a objects
change_machine = MoneyMachine()
coffee_maker = CoffeeMaker()
my_menu = Menu()

#check if machine is on True = On
machine_state = True

while machine_state:
    op = my_menu.get_items()
    user_choice = input(f"Select an option:{op}")
    if user_choice == 'off':
        machine_state = False
    elif user_choice == 'report':
        coffee_maker.report()
        change_machine.report()
    else:
        user_drink = my_menu.find_drink(user_choice)
        check_ingredients = coffee_maker.is_resource_sufficient(user_drink)
        check_payment = change_machine.make_payment(user_drink.cost)
        if check_payment and check_ingredients:
            coffee_maker.make_coffee(user_drink)
