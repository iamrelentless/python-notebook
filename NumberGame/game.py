import random 

number = random.randint(1,100)

total_guesses = 10 #global variable

""" user to check user input (first letter)
and return a boolean """
def get_difficulty(diff):
    if diff[0] == "e":
        return True
    elif diff[0] == "h":
        return False

""" get user guess and return an int value  """
def get_guess():
    guess = int(input("Enter a number between 1-100: "))
    return guess

""" takes a boolean argument and returns an in int.
used to get how many attempts will get """
def guess_ctr(get_diff):
    if get_diff == True:
        return 10 
    elif get_diff == False:
        return 5 

def main():
    set_diff = input("Choose difficulty:'Easy' or 'Hard'").lower()
    counter = guess_ctr(get_difficulty(set_diff))

    print(f"Difficulty Set To: {set_diff}")

    print(f"Total guess: {counter}")

    index = 0 #index for while loop
    total_guess = counter  #used to decrease remaining guess
    while index < counter:
        user_input = get_guess()
        if user_input < number:
            print("Too low")
        elif user_input > number:
            print("Too High")
        elif user_input == number:
            print("Guessed right")
            break #breaks out of the loop once the user guess correctly
        total_guess -= 1
        print(f"Guess remaining: {total_guess}")
        index +=1

    print(f"Number is: {number}\n")

main() #run main function