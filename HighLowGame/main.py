from game_data import data
import random
import os #clear

def get_random_acct():
    return random.choice(data) #from game_data dict

""" format data so it is easier to read in console """
def place_data(acct):
    name = acct["name"]
    desc = acct["description"]
    country = acct["country"]
    return f"{name}, a {desc}, from {country}"

""" checks for followers against user's guess and returns true 
if correct and false if wrong """
def get_answer(guess, followers_a, followers_b):
    if followers_a > followers_b:
        return guess == "a"
    else:
        return guess == "b"


def main():
    score = 0
    end_game = True

    acct_A = get_random_acct()
    acct_B = get_random_acct()

    while end_game:
        acct_A = acct_B
        acct_B = get_random_acct()

        while acct_A == acct_B:
            acct_B = get_random_acct()    
        print(f"Compare A: {place_data(acct_A)}.")
        print("VS")
        print(f"Compare B: {place_data(acct_B)}.")

        guess = input("Who has more followers? Type A or B: ").lower()

        a_acct_guess = acct_A["follower_count"]
        b_acct_guess = acct_B["follower_count"]
        if_correct = get_answer(guess, a_acct_guess, b_acct_guess)

        os.system('cls' if os.name == 'nt' else 'clear')
        if if_correct:
            score+=1
            print(f"Your Score: {score}.")
        else:
            end_game = False
            print(f"Your Score: {score}")


main()