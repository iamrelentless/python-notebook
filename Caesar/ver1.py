alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n") #choice 
text = input("Type your message:\n").lower() #get user message
shift = int(input("Type the shift number:\n")) #amount to shift by

#TODO-1: Create a function called 'encrypt' that takes the 'text' and 'shift' as inputs.
def encrypt(p_text, i_shift):
    cipher_text = " "
    for letters in text:
        #letter is iter location
        ''' index searches for a given element from start of the list and
        returns the lowest index where the element appears.'''
        pos = alphabet.index(letters)
        shift_pos = pos + i_shift
        shift_letter = alphabet[shift_pos]
        cipher_text += shift_letter
    print(f"The encoded text is {cipher_text}")

def decode(cip_text, total_shift):
    plain_text = " "
    for letters in cip_text:
        pos = alphabet.index(letters)
        n_pos =  pos - total_shift # new position
        plain_text += alphabet[n_pos]
    print(f"The decode text is {plain_text}")

'''
this function will take the user input and check if it is a valid choice
'''
def check_user_choice(direct):
    if direct == "encode":
        encrypt(p_text=text, i_shift=shift)
    elif direct == "decode":
        decode(cip_text=text, total_shift=shift)
    else:
        print("No known direction.")

check_user_choice(direct=direction)
