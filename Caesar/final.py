from art import logo
alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

def caesar(message_text, total_shift, cipher_d):
    '''
    function gets message, shift, and whether to encode or decode
    e_text is an empty string and that adds the shifted alphabet
    values
    '''
    e_text = ""
    if cipher_d == 'decode':
        total_shift *= -1
    for chars in message_text:
        if chars in alphabet:
            pos = alphabet.index(chars)
            n_pos = pos + total_shift
            e_text += alphabet[n_pos]
        else:
            e_text += chars
    print(f"Here's the {cipher_d} result: {e_text}")

print(logo)
end_game = False

while not end_game:
    '''
    run cipher
    while loop takes user input for text, shift and, to either encode or decode 
    a message. 
    the loop continues to run until answer to restart is no
    '''
    direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n") #choice
    text = input("Type your message:\n").lower() #get user message
    shift = int(input("Type the shift number:\n")) #amount to shift by
    shift = shift % 26

    #call function
    caesar(message_text=text, total_shift=shift, cipher_d=direction)
    restart = input("Restart Yes/No\n")
    if restart == 'no':
        end_game = True
        print("Closing...")