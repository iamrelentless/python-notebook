from prettytable import PrettyTable


table = PrettyTable() #pretty table object
table.add_column("Player Name", ["Amy Ozee", "Ari Homayun", "Julia Scoles"])
table.add_column("Number", [3, 1, 31])
print(table) #test print
table.align = "l" #align is usually set to center
print(table)