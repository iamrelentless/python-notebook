'''
menu is a dictionary within a dictionary 
coffe type { ingredients {}, } the pair of the coffee type is the cost
'''
menu = {
    "espresso": {
        "ingredients": {
            "water": 50,
            "coffee": 18,
        },
        "cost": 1.5,
    },
    "latte": {
        "ingredients": {
            "water": 200,
            "milk": 150,
            "coffee": 24,
        },
        "cost": 2.5,
    },
    "cappuccino": {
        "ingredients": {
            "water": 250,
            "milk": 100,
            "coffee": 24,
        },
        "cost": 3.0,
    }
}

resources = { #dictionary kvp
    "water": 300,
    "milk": 200,
    "coffee": 100
}

#variables
earned = 0
machine_state = True
    
def check_resources(order_ingredients):
    """ Returns true if order can be made, false if not enough ingredients"""
    for i in order_ingredients:
        if order_ingredients[i] > resources[i]:
            print(f"Not enough resources {i}.\n")
            return False
    return True

def get_payment():
    """ adds up collected coins and return total"""
    print("Enter Coins")
    total_coins = int(input("Quarters:")) * .25
    total_coins += int(input("Dimes:")) * .1
    total_coins += int(input("Nickles:")) * .05
    total_coins += int(input("Pennies:")) * .01
    return total_coins

def set_transaction(moneys, total_cost):
    """return true if payment is accepted, false when not"""
    if moneys >= total_cost:
        change = round(moneys - total_cost, 2) #round by 2 decimal values
        print(f"Total change: {change}")
        global earned
        earned += total_cost
        return True
    else:
        print("Not enough coins. Refunded.")
        return False

def print_report():
    print(f"Water: {resources['water']}ml")
    print(f"Milk: {resources['milk']}ml")
    print(f"Coffee: {resources['coffee']}g")
    print(f"Money: ${earned}")

def make_coffee(drink, get_ingredients):
    """ gets the type of drink, and then gets the ingredients needed
    to make that drink. """
    for i in get_ingredients:
        resources[i] -= get_ingredients[i]
    print(f"Your drink: {drink}")

while machine_state:
    '''
    while loop checks for user input for coffee type, until machine_state turns True
    then it will shut off machine. 
    '''
    user_choice = input("Choose a drink (Espresso/Latte/Cappuccino)").lower()
    if user_choice == "off":
        machine_state = False
    elif user_choice == "report":
        print_report()
    else:
        drink = menu[user_choice]
        if check_resources(drink["ingredients"]):
            pay = get_payment()
            if set_transaction(pay, drink["cost"]):
                make_coffee(user_choice, drink["ingredients"])