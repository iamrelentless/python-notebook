import random 
from art import stages, logo #using linked file
from wordlist import word_list
import os

print(logo) #print logo from art file
game_over = False
lives = len(stages) - 1

chosen_word = random.choice(word_list)
word_length = len(chosen_word) #should be int value

display = [] #displays a list of blank spaces and guess word
for _ in range(word_length):
    display += "_"

while not game_over:
    guess = input("Guess a letter: ").lower()

    #clear() function imported fro replit to clear the output
    #between guesses
    os.system('cls' if os.name == 'nt' else 'clear') 
    if guess in display:
        print(f"You've already guessed {guess}")
    
    for pos in range(word_length):
        letter = chosen_word[pos]
        if letter == guess:
            display[pos] = letter
    print(f"{' '.join(display)}")

    if guess not in chosen_word:
        #print(f"You guessed {guess}, is not in the word." )
        lives -= 1
        if lives == 0:
            game_over = True
            print("\nYou lose.")
            print(f"\nThe word was {chosen_word}\n")

    if not "_" in display:
        game_over = True
        print("You win.")
    print(stages[lives])