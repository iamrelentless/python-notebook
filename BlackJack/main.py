import random 
import os
from art import logo

def deal_card():
    """ returns a random card from the deck  """
    cards = [11,2,3,4,5,6,7,8,9,10,10,10,10]
    card = random.choice(cards) #var to randomly get a card from the list
    return card

#takes a list as an argument
def calc_score(cards):
    '''
    takes a list of cards as an argument
    sums card to check if 21 and if there are only 2 cards in the list
    '''
    if sum(cards) == 21 and len(cards) == 2:
        return 0
    if 11 in cards and sum(cards) > 21:
        cards.remove(11) #remove from list
        cards.append(1) #add to end of list
    return sum(cards) #sum function from python

def compare(user_score, comp_score):
    '''
    compares user score and computer score 
    to check who wins the hand. 
    '''
    if user_score > 21 and comp_score > 21:
        return "Over 21. Lost hand."
    
    if user_score == comp_score:
        return "Draw"
    elif comp_score == 0:
        return "Lose, opponent has Blackjack"
    elif user_score == 0:
        return "Winner, Winner, Chicken Dinner!!!"
    elif user_score > 21:
        return "Over 21. Lost hand!"
    elif comp_score > 21:
        return "Opponent over 21. Winner winner."
    elif user_score > comp_score:
        return "Hand is greater. Winner, Winner!"
    else:
        return "You lost!!!"

def run_game():
    '''
    function runs the game 
    '''
    print(logo)

    user_cards = [] #list
    comp_cards = [] #list
    game_over = False

    '''
    for loop to deal 2 cards to user and computer
    '''
    for _ in range(2):
        """ adds list from deal cards into list"""
        user_cards.append(deal_card())
        comp_cards.append(deal_card())
    while not game_over:
        
        user_score = calc_score(user_cards) #uses calc_score function
        comp_score = calc_score(comp_cards) #uses calc_score function
        print(f"Your cards: {user_cards}, current score: {user_score} ")
        print(f"Computer Initial Card: {comp_cards[0]} ")

        if user_score == 0 or comp_score == 0 or user_score > 21:
            game_over = True
        else:
            user_deal = input("Get another card or pass (y or no) ").lower()
            if user_deal == 'y':
                user_cards.append(deal_card())
            else:
                game_over = True
    #compares scores
    while comp_score != 0 and comp_score < 17:
        comp_cards.append(deal_card())
        comp_score = calc_score(comp_cards)

    print(f"User final hand: {user_cards}, final score: {user_score}")
    print(f"Opponent final hand: {comp_cards}, final score: {comp_score}")
    print(compare(user_score, comp_score))

while input("Play Blackjack, Y or N ").lower() == 'y':
    os.system('cls' if os.name == 'nt' else 'clear')
    run_game()