class User:
    #constructor
    """ values after self are attribute setters for objects """
    def __init__(self, user_id, username):
    #pass #ignore and go to next line of code
    # add attributes
        self.id = user_id
        self.username = username 
        self.followers = 0  #default value
        self.following = 0

    def follow(self, user):
        user.followers += 1 #from param
        self.following += 1

user_1 = User("001", "Barry")

#print(user_1.followers)

user_2 = User("002", "Iris")
user_1.follow(user_2)
print("User 1")
print(user_1.followers)
print(user_1.following)

print("User 2")
print(user_2.followers)
print(user_2.following)